<?php

namespace App;

use App\Refaccion;
use App\OrdenTrabajo;
use App\HerramientasTaller;
use App\ProductoReparacion;
use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    //
    protected $fillable = [
        'idProductoReparacion','idRefacciones','idHerramientasTaller','tipo_movimiento'
    ];

    public function herramienta(){
        return $this->belongsTo(HerramientasTaller::class);
    }

    public function producto(){
        return $this->belongsTo(ProductoReparacion::class);
    }

    public function refaccione(){
        return $this->belongsTo(Refaccion::class);
    }
    public function ordenT(){
        return $this->belongsTo(OrdenTrabajo::class);
    }

}
