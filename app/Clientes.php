<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Descuento;
use App\Trasporte;
class Clientes extends Model
{
    //
    protected $fillable = [
        'nombre','apellido_p','apellido_m','rfc','forma_pago','dirección','colonia','codigo_postal',
        'ciudad','telefono','celular','email','observaciones','no_cliente'
    ];

    public function descuento(){
        return $this->belongsToMany(Descuento::class);
    }

    public function trasportes(){
        return $this->hasMany(Trasporte::class);
    }
}
