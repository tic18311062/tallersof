<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuento extends Model
{
    //
    protected $fillable = [
        'tipo_descuento','idCliente'
    ];

    public function cliente(){
        return $this->hasMany(Clientes::class);
    }
}
