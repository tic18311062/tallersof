<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Almacen;
class HerramientasTaller extends Model
{
    //
    protected $fillable = [
        'codigo_barra','nombre','descripcion','tipo_unidad','cantidad','costo_compra'
    ];

    public function almacen(){
        return $this->belongsToMany(Almacen::class);
    }
}
