<?php

namespace App;
use App\OrdenTrabajo;
use Illuminate\Database\Eloquent\Model;

class Mecanico extends Model
{
    //
    protected $fillable = [
        'nombre','apellido_p','apellido_m','rfc','dirección','colonia','codigo_postal','ciudad',
        'telefono','celular','email','observaciones','foto_mecanico'
    ];

    public function OrdenTrabajo(){
        return $this->belongsToMany(OrdenTrabajo::class);
    }
}
