<?php

namespace App;

use App\OrdenTrabajo;
use App\Refaccion;
use Illuminate\Database\Eloquent\Model;

class RefaccionOrdenTrabajo extends Model
{
    //
    protected $fillable = [
        'idRefacciones','idOrdenTrabajo'
    ];

    public function refacciones(){
        return $this->hasMany(Refaccion::class);
    }
    public function ordenT(){
        return $this->hasMany(OrdenTrabajo::class);
    }
}
