<?php

namespace App;
use App\TrabajoDefinidoOrdenTrabajo;
use Illuminate\Database\Eloquent\Model;

class TrabajoDefinido extends Model
{
    //
    protected $fillable = [
        'nombre','descripcion','precio'
    ];

    public function trabajoDefinidoOrdenT(){
        return $this->belongsToMany(TrabajoDefinidoOrdenTrabajo::class);
    }
}
