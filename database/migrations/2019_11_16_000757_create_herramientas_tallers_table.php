<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHerramientasTallersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('herramientas_tallers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo_barra',255);
            $table->string('nombre',45);
            $table->text('descripcion');
            $table->string('tipo_unidad',45);
            $table->integer('cantidad');
            $table->double('costo_compra');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('herramientas_tallers');
    }
}
