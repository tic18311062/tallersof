<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMecanicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mecanicos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',45);
            $table->string('apellido_p',45)->nullable();
            $table->string('apellido_m',45)->nullable();
            $table->string('rfc',13)->unique();
            $table->string('dirección',255);
            $table->string('colonia',100);
            $table->string('codigo_postal',45);
            $table->string('ciudad',45);
            $table->integer('telefono')->nullable();
            $table->integer('celular')->nullable();
            $table->string('email',45);
            $table->text('observaciones')->nullable();
            $table->string('foto_mecanico',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mecanicos');
    }
}
