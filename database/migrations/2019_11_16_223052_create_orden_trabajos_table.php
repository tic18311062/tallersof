<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_trabajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idCliente')->unsigned();
            $table->foreign('idCliente')->references('id')->on('clientes');
            $table->enum('tipo_orden',array('NORMAL','RESCATE','GARANTÍA','R&G_AMBOS'));
            $table->bigInteger('idTrasporte')->unsigned();
            $table->foreign('idTrasporte')->references('id')->on('trasportes');
            $table->enum('esterio',array('on','off'))->nullable();
            $table->enum('cb',array('on','off'))->nullable();
            $table->enum('antena_cb',array('on','off'))->nullable();
            $table->enum('barras_cont',array('on','off'))->nullable();
            $table->enum('vcr',array('on','off'))->nullable();
            $table->enum('monitores',array('on','off'))->nullable();
            $table->enum('maleteros',array('on','off'))->nullable();
            $table->enum('ceniceros',array('on','off'))->nullable();
            $table->enum('funda_asientos',array('on','off'))->nullable();
            $table->integer('tanque')->default(5);
            $table->string('base64',255)->nullable();
            $table->string('serie_orden_de_trabajo',50)->unique()->nullable();
            $table->date('fechaEmision')->nullable();
            $table->date('fechaPrometida')->nullable();
            $table->text('reporteInicial');
            $table->text('obcervaciones')->nullable();
            $table->bigInteger('idMecanico')->unsigned();
            $table->foreign('idMecanico')->references('id')->on('mecanicos');
            $table->integer('kilometraje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_trabajos');
    }
}
