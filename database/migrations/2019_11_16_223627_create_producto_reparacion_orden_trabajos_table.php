<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoReparacionOrdenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_reparacion_orden_trabajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idProductoReparacion')->unsigned();
            $table->foreign('idProductoReparacion')->references('id')->on('producto_reparacions');
            $table->bigInteger('idOrdenTrabajo')->unsigned();
            $table->foreign('idOrdenTrabajo')->references('id')->on('orden_trabajos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_reparacion_orden_trabajos');
    }
}
