<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefaccionOrdenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refaccion_orden_trabajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idRefacciones')->unsigned();
            $table->foreign('idRefacciones')->references('id')->on('refaccions');
            $table->bigInteger('idOrdenTrabajo')->unsigned();
            $table->foreign('idOrdenTrabajo')->references('id')->on('orden_trabajos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refaccion_orden_trabajos');
    }
}
