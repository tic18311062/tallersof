<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrabajoDefinidoOrdenTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajo_definido_orden_trabajos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idTrabajoDefinido')->unsigned();
            $table->foreign('idTrabajoDefinido')->references('id')->on('trabajo_definidos');
            $table->bigInteger('idOrdenTrabajo')->unsigned();
            $table->foreign('idOrdenTrabajo')->references('id')->on('orden_trabajos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajo_definido_orden_trabajos');
    }
}
