<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlmacensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almacens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idProductoReparacion')->unsigned();
            $table->foreign('idProductoReparacion')->references('id')->on('producto_reparacions');
            $table->unsignedBigInteger('idRefacciones');
            $table->foreign('idRefacciones')->references('id')->on('refaccions');
            $table->bigInteger('idHerramientaTaller')->unsigned();
            $table->foreign('idHerramientaTaller')->references('id')->on('herramientas_tallers');
            $table->string('idOrdenT',50)->nullable();
            $table->foreign('idOrdenT')->references('serie_orden_de_trabajo')->on('orden_trabajos');
            $table->enum('tipo_movimiento',array('ENTRADA_POR_COMPRA', 'ENTRADA_POR_DEVOLUCION', 'SALIDA_POR_VENTA', 'SALIDA_POR_ORDENT'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('almacens');
    }
}
