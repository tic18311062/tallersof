@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container tarlogin">
        <div class="row">
            <div class="col-12 aling-right">
                <button class="btn" type="button" data-toggle="collapse"
                data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                style="width: 50px; height: 30px;" >
                    <img class="card-img-top" src="{{ asset('img/minimiza.svg')}}" alt="">
                </button>
            </div>
        </div>
        <div class="row justify-content-center" id="collapseExample">
            <div class="col-xl-4 col-ms-4">
                <div class="form-group">
                    <input type="text"
                    class="form-control" name="no_cliente" id="no_cliente" placeholder="Codigo de barra">
                </div>
            </div>
            <div class="col-xl-4 col-ms-4">
                <div class="form-group">
                    <input type="text"
                    class="form-control" name="no_cliente" id="no_cliente" placeholder="Nombre">
                </div>
            </div>
            <div class="col-xl-4 col-ms-4">
                <div class="form-group">
                    <button type="button" class="btn btn-info btn-sm">Buscar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="col-xl-4 col-ms-4">
            <a class="btn btn-light border border-primary" href="{{Route('herramientas.create')}}" role="button">Herramientas Taller</a>
        </div>
        <div class="col-xl-4 col-ms-4">
            <a class="btn btn-light border border-primary" href="{{Route('producto.create')}}" role="button">Producto Reparacion</a>
        </div>
        <div class="col-xl-4 col-ms-4">
            <a class="btn btn-light border border-primary" href="{{Route('refaccion.create')}}" role="button">Refaccion</a>
        </div>
    </div>
</div>
<br>
<div class="container">
    <table class="table table-hover table-inverse table-responsive">
        <thead class="thead-inverse">
            <tr>
                <td>Codigo Barras</td>
                <th>Nombre</th>
                <th>Unidad</th>
                <th>Cantidad</th>
                <th>Costo</th>
                <th>Venta</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($data['producto'] as $pro)      
                    <tr>
                        <td>{{$pro->codigo_barra}}</td>
                        <td>{{$pro->nombre}}</td>
                        <td>{{$pro->tipo_unidad}}</td>
                        <td>{{$pro->cantidad}}</td>
                        <td>{{$pro->costo_compra}}</td>
                        <td>{{$pro->costo_venta}}</td>
                    </tr>
                @endforeach
                @foreach ($data['refaccion'] as $refa)      
                    <tr>
                        <td>{{$refa->codigo_barra}}</td>
                        <td>{{$refa->nombre}}</td>
                        <td>{{$refa->tipo_unidad}}</td>
                        <td>{{$refa->cantidad}}</td>
                        <td>{{$refa->costo_compra}}</td>
                        <td>{{$refa->costo_venta}}</td>
                    </tr>
                @endforeach
                @foreach ($data['herramienta'] as $herra)      
                    <tr>
                        <td>{{$herra->codigo_barra}}</td>
                        <td>{{$herra->nombre}}</td>
                        <td>{{$herra->tipo_unidad}}</td>
                        <td>{{$herra->cantidad}}</td>
                        <td>{{$herra->costo_compra}}</td>
                    </tr>
                @endforeach
            </tbody>
    </table>
</div>
@endsection
