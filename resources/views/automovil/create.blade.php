@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <form action="{{ Route('automovil.store')}}" method="post">
        @csrf
        <div class="container">
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <label for="idCliente">No.Cliente *</label>
                            <input type="text" name="idCliente" id="idCliente" class="form-control" placeholder="Numero Cliente" required>
                        </div>
                        <div class="col-6  align-self-center pt-3">
                            <p class="pt-3">
                                <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#mostraCliente" aria-expanded="false" aria-controls="mostraCliente">
                                    Clientes.....
                                </a>
                            </p>
                            <div class="collapse" id="mostraCliente">
                                <table class="table table-hover table-inverse table-responsive">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>id</th>
                                            <th>Nombre</th>
                                            <th>Apellido P</th>
                                            <th>Apellido M</th>
                                            <th>Opcion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($clientes as $cliente)
                                                <tr>
                                                    <td scope="row">{{  $cliente->id }}</td>
                                                    <td>{{  $cliente->nombre }}</td>
                                                    <td>{{  $cliente->apellido_p }}</td>
                                                    <td>{{  $cliente->apellido_m }}</td>
                                                    <td>
                                                        <button type="button" name="" id="cliente_{{  $cliente->id }}" class="btn btn-info btn-sm btn-block" onclick="selccionarCliente(this.id);">Select</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="container tarlogin">
            <div class="row">
                <div class="col-xl-12">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="car1" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="car1">Carros</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="car3" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="car3">Camiones de carga</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                      <label for="marca">Marca</label>
                      <select class="form-control" name="marca" id="marca">
                        @foreach ($marcas as $marca)
                            <option value="{{$marca['Value']}}">{{$marca['Label']}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="modelo">Modelo</label>
                        <select class="form-control" name="modelo" id="modelo">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                      <label for="ano">Año</label>
                      <select class="form-control" name="ano" id="ano">
                        <option value="0">Año</option>""
                        @for ($i = 1945; $i < 2019+1; $i++)
                            <option >{{$i}}</option>
                        @endfor
                      </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <label for="no_eco">No.Eco</label>
                    <input type="text" name=no_eco" id="no_eco" class="form-control" placeholder="Numero ECO">
                </div>
                <div class="col-6">
                    <label for="serie">No.Serie</label>
                    <input type="text" name="serie" id="serie" class="form-control" placeholder="Numero Serie">
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="form-group">
                    <label for="observaciones">Observaciones</label>
                    <textarea class="form-control" name="observaciones" id="observaciones" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-info">Registrar</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('script_A')
    <script>    
        $('#marca').change(function(){
            //alert('change select '+$('#marca').val());
            console.log('log de change');
            $.ajax({
                method: "POST",
                url: "http://veiculos.fipe.org.br/api/veiculos/ConsultarModelos",
                data: { 
                    codigoTabelaReferencia: 231, 
                    codigoTipoVeiculo: 1 ,
                    codigoMarca: $('#marca').val() ,
                }
                 }).done(function(data) {  
                    // alert('terimnao ' + data['Modelos'][0]['Label']);
                    $('#modelo').empty();
                    for(var k in data['Modelos']) {
                      // alert(k, data['Modelos'][k]['Label']);
                       $('#modelo').append("<option value="+data['Modelos'][k]['Value']+">"+data['Modelos'][k]['Label']+"</option>");
                    }
                });
            });
    </script>
@endsection
