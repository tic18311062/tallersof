@extends('layouts.app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <a class="btn btn-light border border-primary" href="{{Route('Clientes.create')}}" role="button">Nuevo Cliente</a>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <table class="table table-hover table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>NO.Cliente</th>
                        <th>Nombre</th>
                        <th>Apellido P</th>
                        <th>Apellido M</th>
                        <th>RFC</th>
                        <th>Opcion</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->no_cliente}}</td>
                            <td>{{ $cliente->nombre}}</td>
                            <td>{{ $cliente->apellido_p}}</td>
                            <td>{{ $cliente->apellido_m}}</td>
                            <td>{{ $cliente->rfc}}</td>
                            <td>
                                <button type="button" name="" id="cliente_{{ $cliente->id }}" onclick="selccionarCliente(this.id);" class="btn btn-info btn-sm btn-block">Seleccionar</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
@endsection
