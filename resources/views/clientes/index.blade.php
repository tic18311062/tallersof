@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="container tarlogin">
            <div class="row">
                <div class="col-12 aling-right">
                    <button class="btn" type="button" data-toggle="collapse"
                     data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                     style="width: 50px; height: 30px;" >
                          <img class="card-img-top" src="{{ asset('img/minimiza.svg')}}" alt="">
                    </button>
                </div>
            </div>
            <form action="" method="post">
                @csrf
                <div class="row" id="collapseExample">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="no_cliente">No.Cliente</label>
                            <input type="text"
                            class="form-control" name="no_cliente" id="no_cliente" placeholder="Numero Cliente">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                        <label for="rfc">RFC</label>
                        <input type="text" class="form-control" name="rfc" id="rfc" placeholder="RFC">
                        </div>
                    </div>
                    <div class="col-3 align-self-center pt-3">
                        <button type="submit" class="btn btn-light">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <a class="btn btn-light border border-primary" href="{{Route('Clientes.create')}}" role="button">Nuevo Cliente</a>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <table class="table table-hover table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>NO.Cliente</th>
                        <th>Nombre</th>
                        <th>Apellido P</th>
                        <th>Apellido M</th>
                        <th>RFC</th>
                        <th rowspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->no_cliente}}</td>
                            <td>{{ $cliente->nombre}}</td>
                            <td>{{ $cliente->apellido_p}}</td>
                            <td>{{ $cliente->apellido_m}}</td>
                            <td>{{ $cliente->rfc}}</td>
                            <td>
                                <form action="{{ route('Clientes.show',$cliente->id) }}" method="get">
                                    @csrf
                                    <button type="submit" class="btn btn-info">Ver</button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('Clientes.destroy',$cliente->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                        <input type="hidden" name="id" id="id" value="{{ $cliente->id }}">
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
@endsection
