@extends('layouts.app')
@section('content')
<div class="container">
    <table class="table table-hover table-inverse table-responsive">
        <thead class="thead-inverse">
            <tr>
                <th>id</th>
                <th>No.Cliente</th>
                <th>Nombre</th>
                <th>RFC</th>
                <th>Descuento</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($data as $date)
                <tr>
                    <td scope="row">{{ $date->id }}</td>
                    <td>{{ $date->no_cliente}}</td>
                    <td>{{ $date->nombre}}</td>
                    <td>{{ $date->rfc}}</td>
                    <td>{{ $date->descuento}}</td>
                </tr>
                @endforeach
            </tbody>
    </table>
</div>
@endsection
