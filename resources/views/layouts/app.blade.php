
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TallerSof') }}</title>

    <!-- Scripts -->
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/canvas.js') }}" defer></script>
    <script src="{{ asset('js/generadorOT.js') }}" defer></script>
    <script src="{{ asset('js/tanque.js') }}" defer></script>
    <script src="{{ asset('js/canvasfurgo.js') }}" defer></script>
    <script src="{{ asset('js/seleccionar.js') }}" defer></script>
    <script src="{{ asset('js/guardarCanvas.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mio.css') }}" rel="stylesheet">
    <script>
                function pepe(){
            if(localStorage.getItem('idcliente')!=null){
                console.log('entro'+localStorage.getItem('idcliente'));
                document.getElementById('idcliente').value=localStorage.getItem('idcliente');
            }
        }
    </script>
</head>
<body onpageshow="pepe();">
    <div id="app banner">
        <nav class=" navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                {{-- <!--logo o nombre
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                --> --}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @if (Auth::user())
                            <li class="nav-item">
                                <a name="" id="" class="btn" href="{{route('almacen.index')}}" role="button">Almacen</a>
                            </li>
                        @endif
                        @if (Auth::user()['tipo_us']=='ADMINISTRATIVO'||Auth::user()['tipo_us']=='JEFE_TALLER'||Auth::user()['tipo_us']=='ADMIN')    
                            <li class="nav-item">
                                <a name="" id="" class="btn" href="{{ Route('Clientes.index') }}" role="button">Clientes</a>
                            </li>
                        @endif
                        @if (Auth::user()['tipo_us']=='ADMINISTRATIVO'||Auth::user()['tipo_us']=='ADMIN')                            
                            <li class="nav-item">
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="utilidadesid" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                                Utilidades
                                            </button>
                                    <div class="dropdown-menu" aria-labelledby="utilidadesid">
                                        <a class="dropdown-item" href="{{ Route('descuento.create') }}">Dar Descuento</a>
                                        <a class="dropdown-item" href="{{ Route('descuento.index') }}">Ver Descuentos</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Administrativo</a>
                                    </div>
                                </div>
                            </li>
                        @endif
                        @if (Auth::user())    
                            <li class="nav-item">
                                <div class="dropdown">
                                        <button class="btn dropdown-toggle" type="button" id="catalogoid" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                            Catalogo
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="catalogoid">
                                            @if (Auth::user()['tipo_us']=='JEFE_TALLER'||Auth::user()['tipo_us']=='ADMIN')    
                                                <a class="dropdown-item" href="{{ Route('automovil.index') }}">Automovil</a>
                                            @endif
                                            <div class="dropdown-divider"></div>
                                            @if (Auth::user()['tipo_us']=='ADMINISTRATIVO'||Auth::user()['tipo_us']=='ADMIN')
                                                <a class="dropdown-item" href="{{ Route('mecanico.index') }}">Mecanico</a>
                                            @endif
                                        </div>
                                </div>
                            </li>
                        @endif
                        @if (Auth::user()['tipo_us']=='ADMIN'||Auth::user()['tipo_us']=='JEFE_TALLER')    
                            <li class="nav-item">
                                <div class="dropdown">
                                        <button class="btn dropdown-toggle" type="button" id="ordenTid" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                        Orden Trabajo
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="ordenTid">
                                        <a class="dropdown-item" href="/opciones">Nueva OrdenT</a>
                                        <a class="dropdown-item" href="{{ Route('ordentrabajo.index') }}">Buscar Orden</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ Route('trabajo.index') }}">Trabajo Definidos</a>
                                    </div>
                                </div>
                            </li>
                        @endif
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @yield('script_A')
</body>

</html>
