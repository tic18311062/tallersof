@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <a class="btn btn-light border border-primary" href="{{Route('mecanico.create')}}" role="button">Nuevo Mecanico</a>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <table class="table table-hover table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th>Nombre</th>
                    <th>Apellido P</th>
                    <th>Apellido M</th>
                    <th rowspan="2">Opcion</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($mecanico as $mecani)
                        <tr>
                            <td>{{ $mecani->nombre }}</td>
                            <td>{{ $mecani->apellido_p }}</td>
                            <td>{{ $mecani->apellido_m }}</td>
                            <td>
                                <a name="" id="" class="btn btn-info" href="#" target="_blank" role="button">Seleccionar</a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
        </table>
    </div>
@endsection
