@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        <form action="{{ Route('mecanico.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-6">
                        <label for="rfc">RFC *</label>
                        <input type="text" name="rfc" id="rfc" class="form-control" placeholder="RFC" required>
                    </div>
                    <div class="col-6">
                        <label for="nombre">Nombre *</label>
                        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <label for="apellido_p">Apellido P</label>
                        <input type="text" name="apellido_p" id="apellido_p" class="form-control" placeholder="Apellido Paterno">
                    </div>
                    <div class="col-6">
                        <label for="apellido_m">Apellido M</label>
                        <input type="text" name="apellido_m" id="apellido_m" class="form-control" placeholder="Apellido Materno">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label for="dirección">Dirección *</label>
                        <input type="text" name="dirección" id="dirección" class="form-control" placeholder="Dirección" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="colonia">Colonia * </label>
                        <input type="text" name="colonia" id="colonia" class="form-control" placeholder="Direccion" required>
                    </div>
                    <div class="col-4">
                        <label for="codigo_postal">Codigo Postal *</label>
                        <input type="text" name="codigo_postal" id="codigo_postal" class="form-control" placeholder="codigo postal" required>
                    </div>
                    <div class="col-4">
                        <label for="ciudad">Ciudad *</label>
                        <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ciudad" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="telefono">Telefono</label>
                        <input type="text"class="form-control" name="telefono" id="telefono" placeholder="Telefono">
                    </div>
                    <div class="col-4">
                        <label for="movil">Movil</label>
                        <input type="text"class="form-control" name="movil" id="movil" placeholder="Telefono">
                    </div>
                    <div class="col-4">
                        <label for="email">Email *</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Correo Electronico" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <label for="foto_mecanico">Foto Mecanico *</label>
                        <input type="file" name="foto_mecanico" class="form-control" id="foto_mecanico" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group">
                          <label for="observaciones">Observaciones</label>
                          <textarea class="form-control" name="observaciones" id="observaciones" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-info">Registrar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
