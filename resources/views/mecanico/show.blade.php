@extends('layouts.app')
@section('content')
<?php
$meca=$_GET['id_mecanico'];
?>
    <div class="container">
        <form action="{{ Route('mecanico.update',$meca)}}" method="post">
            <div class="row">
                <div class="col-4">
                    <img src="{{ asset( $mecanico->foto_mecanico )}}" class="img-fluid rounded-circle rounded-lg" alt="">
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="rfc">RFC </label>
                                <input type="text" name="rfc" id="rfc" class="form-control" placeholder="RFC" value="{{ $mecanico->rfc }}">
                            </div>
                            <div class="col-6">
                                <label for="nombre">Nombre </label>
                                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre" value="{{ $mecanico->nombre }}" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label for="apellido_p">Apellido P</label>
                                <input type="text" name="apellido_p" id="apellido_p" class="form-control" placeholder="Apellido Paterno" value="{{ $mecanico->apellido_p }}">
                            </div>
                            <div class="col-6">
                                <label for="apellido_m">Apellido M</label>
                                <input type="text" name="apellido_m" id="apellido_m" class="form-control" placeholder="Apellido Materno" value="{{ $mecanico->apellido_m }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label for="dirección">Dirección *</label>
                                <input type="text" name="dirección" id="dirección" class="form-control" placeholder="Dirección" value="{{ $mecanico->dirección }}" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label for="colonia">Colonia  </label>
                                <input type="text" name="colonia" id="colonia" class="form-control" placeholder="Direccion" value="{{ $mecanico->colonia }}" >
                            </div>
                            <div class="col-4">
                                <label for="codigo_postal">Codigo Postal</label>
                                <input type="text" name="codigo_postal" id="codigo_postal" class="form-control" placeholder="codigo postal" value="{{ $mecanico->codigo_postal }}">
                            </div>
                            <div class="col-4">
                                <label for="ciudad">Ciudad </label>
                                <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ciudad" value="{{ $mecanico->ciudad }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label for="telefono">Telefono</label>
                                <input type="text"class="form-control" name="telefono" id="telefono" placeholder="Telefono" value="{{ $mecanico->telefono }}">
                            </div>
                            <div class="col-4">
                                <label for="movil">Movil</label>
                                <input type="text"class="form-control" name="movil" id="movil" placeholder="celular" value="{{ $mecanico->movil }}">
                            </div>
                            <div class="col-4">
                                <label for="email">Email </label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Correo Electronico" value="{{ $mecanico->email }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="observaciones">Observaciones</label>
                                    <textarea class="form-control" name="observaciones" id="observaciones" rows="5">{{ $mecanico->observaciones }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-info">Registrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
