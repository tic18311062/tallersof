@extends('layouts.app')
@section('content')
<form action="{{ Route('ordentrabajo.store') }}" method="post" ENCTYPE='multipart/form-data' id="formCan">
    @csrf
<div class="container">
    <div class="row">
        <div class="col-xl-2 col-sm-2 align-items-center">
            <div class="form-group">
                <?php
                    $meca=$_GET['idMecanico'];
                ?><label for="idMecanico">Mecanico: </label>
                <input type="text" class="form-control" name="idMecanico" id="idMecanico" value='{{ $meca }}' onkeydown="event.preventDefault();">
            </div>
        </div>
        <div class="col-xl-2 col-sm-2 align-items-center">
            <div class="form-group">
                <?php
                    $clien=$_GET['idCliente'];
                ?><label for="idCliente">Cliente :</label>
              <input type="text" class="form-control" name="idCliente" id="idCliente" value="{{ $clien }}"  onkeydown="event.preventDefault();">
            </div>
        </div>
        <div class="col-xl-2 col-sm-2 align-items-center">
            <div class="form-group">
                <?php
                    $tras=$_GET['idTrasporte'];
                ?><label for="idTrasporte">Trasporte :</label>
                <input type="text" class="form-control" name="idTrasporte" id="idTrasporte" value="{{ $tras }}" onkeydown="event.preventDefault();">
            </div>
        </div>
    </div>
    <div class="row tarlogin">
        <!--checkBox-->
        <div class="col-xl-5 col-sm-5 align-items-center">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="normal" name="customRadioInline1" class="custom-control-input">
                <label class="custom-control-label" for="normal">Normal</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="rescate" name="customRadioInline1" class="custom-control-input">
                <label class="custom-control-label" for="rescate">Rescate</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="garantia" name="customRadioInline1" class="custom-control-input">
                <label class="custom-control-label" for="garantia">Garantia</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="r&g" name="customRadioInline1" class="custom-control-input">
                <label class="custom-control-label" for="r&g">R&G Ambos</label>
            </div>
        </div>
        <div class="col-xl-5 col-sm-5 align-self-end">
            <!--<p id=demo></p>-->
            <input type="text" name="serie_orden_de_trabajo" id="serie_orden_de_trabajo" placeholder="Serie" onkeydown="event.preventDefault();">
            <button type="button" onclick="randomQuote();"> Generar Orden Serie</button>
        </div>
    </div>
</div>
<!--Canvas-->
<div class="container">
    <div class="row">
        <div class="col-12">
            <button type="button" onclick="change();">draw image</button>
        </div>
    </div>
        <div class="d-none">
            <img src="{{ asset('img/canvas/CheckCamioneta.PNG') }}" id="dummy" class="carroChico" alt="">
        </div>
    <div class="row d-flex">
        <div class="col-xl-6 col-xs-12">
            <canvas id="micanvas" class="canvas-cr"
                onmousemove="pintar(event);" onmouseup="desactivar();" onmousedown="activar();" onload="change();">
            </canvas>
            <input type="hidden" name="base64" value="" id="base64">
        </div>
        <!--CheckList-->
        <div class="col-xl-6 col-xs-12">
            <div id="checkList" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" role="tab" id="section1HeaderId">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-parent="#checkList" href="#section1ContentId" aria-expanded="true" aria-controls="section1ContentId">
                      Check Lists
                    </a>
                        </h5>
                    </div>
                    <div id="section1ContentId" class="collapse in" role="tabpanel" aria-labelledby="section1HeaderId">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="esterio" name="esterio">
                                        <label class="custom-control-label" for="esterio">Esterio</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="cb" name="cb">
                                        <label class="custom-control-label" for="cb">CB</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="antena_cb" name="antena_cb">
                                        <label class="custom-control-label" for="antena_cb">Antena CB</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="barras_cont" name="barras_cont">
                                        <label class="custom-control-label" for="barras_cont">Barras Cont</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="vcr" name="vcr">
                                        <label class="custom-control-label" for="vcr">VCR</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="monitores" name="monitores">
                                        <label class="custom-control-label" for="monitores">monitores</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="ceniceros" name="ceniceros">
                                        <label class="custom-control-label" for="ceniceros">Ceniceros</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="funda_asientos" name="funda_asientos">
                                        <label class="custom-control-label" for="funda_asientos">Funda Asientos</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 pt-2">
                    <div class="form-group">
                        <input type="text" class="form-control" name="kilometraje" id="kilometraje" placeholder="kilometraje">
                    </div>
                </div>
                <div class="col-6">
                    <map name="tanque">
                        <area shape="rect" coords="4,60,35,81" onclick="area1();" alt="tanque 1">
                        <area shape="rect" coords="16,30,42,55" onclick="area2();" alt="tanque 2">
                        <area shape="rect" coords="48,9,71,51" onclick="area3();" alt="tanque 3">
                        <area shape="rect" coords="75,5,93,36" onclick="area4();" alt="tanque 4">
                        <area shape="rect" coords="101,4,120,41" onclick="area5();" alt="tanque 5">
                        <area shape="rect" coords="124,6,147,34" onclick="area6();" alt="tanque 6">
                        <area shape="rect" coords="155,21,178,51" onclick="area7();" alt="tanque 7">
                        <area shape="rect" coords="177,37,198,62" onclick="area8();" alt="tanque 8">
                        <area shape="rect" coords="181,57,213,83" onclick="area9();" alt="tanque 9">
                    </map>
                    <img src="{{ asset('img/tanque/T1.jpg') }}" alt="tanque" usemap="#tanque" id="imgtanque">
                    <input type="hidden" name="tanque" id="tanque" value="">
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-12">
                    <label for="fechaEmision">Fecha Emitida</label>
                    <input type="date" name="fechaEmision" id="fechaEmision">
                </div>
                <div class="col-12">
                    <label for="FechaPrometida">Fecha Prometida</label>
                    <input type="date" name="FechaPrometida" id="FechaPrometida">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                      <label for="reporteInicial">Reporte Inicial</label>
                      <textarea class="form-control" name="reporteInicial" id="reporteInicial" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                      <label for="trabajodefinido">Trabajo Definido</label>
                      <select  class="form-control form-control-sm" name="trabajodefinido" id="trabajodefinido">
                        @foreach ($data['trabajo'] as $traba)
                            <option value="{{$traba->nombre}}">{{$traba->nombre}},{{$traba->descripcion}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="obcervaciones">Observaciones</label>
                        <textarea class="form-control" name="obcervaciones" id="obcervaciones" rows="4"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <button id="saveandfinish" class="btn btn-info" onclick="guardar();">Guardar</button>
        </div>
    </div>
</div>
</form>
@endsection
