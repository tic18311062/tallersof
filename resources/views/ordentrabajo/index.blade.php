@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="container tarlogin">
            <div class="row">
                <div class="col-12 aling-right">
                    <button class="btn" type="button" data-toggle="collapse"
                     data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample"
                     style="width: 50px; height: 30px;" >
                          <img class="card-img-top" src="{{ asset('img/minimiza.svg')}}" alt="">
                    </button>
                </div>
            </div>
            <form action="" method="post">
                @csrf
                <div class="row" id="collapseExample">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="no_cliente">No.Cliente</label>
                            <input type="text"
                            class="form-control" name="no_cliente" id="no_cliente" placeholder="Numero Cliente">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                        <label for="mecanico">Mecanico</label>
                        <input type="text" class="form-control" name="mecanico" id="mecanico" placeholder="Mecanico">
                        </div>
                    </div>
                    <div class="col-3 align-self-center pt-3">
                        <button type="submit" class="btn btn-light">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <a class="btn btn-light border border-primary" href="/opciones" role="button">Nueva Orden de Trabajo</a>
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <table class="table table-hover table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>Id Mecanico</th>
                        <th>Id Cliente</th>
                        <th>Id Trasporte</th>
                        <th>Serie Orden Trabajo</th>
                        <th>Estado</th>
                        <th rowspan="4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($ordenT as $t)
                            <tr>
                                <td>{{$t->idMecanico}}</td>
                                <td>{{$t->idCliente}}</td>
                                <td>{{$t->idTrasporte}}</td>
                                <td>{{$t->serie_orden_de_trabajo}}</td>
                                @foreach ($estados as $estado)
                                    <td>{{$estado->estado}}</td>
                                @endforeach
                                <td>
                                    <form action="{{route('ordentrabajo.show',$t->id)}}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-info btn-sm">Ver</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{route('produ_orden.create')}}" method="GET">
                                        @csrf
                                        <input type="hidden" name="idOrdenTrabajo" value="{{$t->id}}">
                                        <button type="submit" class="btn btn-light border border-primary btn-sm">Agregar Producto</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{route('refa_orden.create')}}" method="GET">
                                        @csrf
                                        <input type="hidden" name="idOrdenTrabajo" value="{{$t->id}}">
                                        <button type="submit" class="btn btn-light border border-primary btn-sm">Agregar Refaccion</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{route('estado.store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="idOrden" value="{{$t->id}}">
                                        <button type="submit" class="btn btn-light border border-primary btn-sm">Cambiar Estado</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
@endsection
