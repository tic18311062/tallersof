@extends('layouts.app')
@section('content')
<form action="{{ route('herramientas.store') }}" method="post">
@csrf
<div class="container">
    <div class="row">
        <div class="col-xl-6">
            <div class="form-group">
                <label for="codigo_barra">Codigo de Barra *</label>
                <input type="text" class="form-control" name="codigo_barra" id="codigo_barra" placeholder="Codigo de barra" required>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="form-group">
                <label for="nombre">Nombre *</label>
                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="form-group">
              <label for="descripcion">Descripcion *</label>
              <textarea class="form-control" name="descripcion" id="descripcion" rows="3" required></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3">
            <div class="form-group">
                <label for="tipo_unidad">Tipo Unidad *</label>
                <input type="text" class="form-control" name="tipo_unidad" id="tipo_unidad" placeholder="Tipo de unidad" required>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="form-group">
                <label for="cantidad">Cantidad *</label>
                <input type="text" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad" required>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="form-group">
                <label for="costo_compra">Costo Compra *</label>
                <input type="text" class="form-control" name="costo_compra" id="costo_compra" placeholder="Costo Compra" required>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="form-group">
                <label for="costo_venta">Costo Venta *</label>
                <input type="text" class="form-control" name="costo_venta" id="costo_venta" placeholder="Costo Venta" required>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-info">Crear</button>
</div>
</form>
@endsection
