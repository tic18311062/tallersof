@extends('layouts.app')
@section('content')
<form action="{{ Route('trabajo.store') }}" method="post">
@csrf
<div class="container">
    <div class="row">
        <div class="col-xl-6">
            <div class="form-group">
              <label for="nombre">Nombre *</label>
              <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre">
            </div>
        </div>
        <div class="col-xl-6">
            <div class="form-group">
              <label for="precio">Precio *</label>
              <input type="text" class="form-control" name="precio" id="precio" placeholder="Precio">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="form-group">
              <label for="descripcion">Descripcion *</label>
              <textarea class="form-control" name="descripcion" id="descripcion" rows="3"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <button type="submit" class="btn btn-info">Crear</button>
        </div>
    </div>
</div>
</form>
@endsection
